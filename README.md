# web-server

1.0.0

- A basic web server in C. Because you never know when you may need one.

1.1.0

- Refactored the code so that the creation of the server socket and client socket are in their own functions.
Cleaned up the main loop and enhanced readability.
