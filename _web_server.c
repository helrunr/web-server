#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

char tst[] = 
"HTTP/1.1 200 OK\r\n"
"Content-Type: text/html; charset=UTF-8\r\n\r\n"
"<!DOCTYPE html>\r\n"
"<html><head><title>Web Server</title></head></html>\r\n";

short create_socket(void)
{
    unsigned int on = 1;
    struct sockaddr_in server_addr;

    int fd_server;

    setsockopt(fd_server, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(int));

    fd_server = socket(AF_INET, SOCK_STREAM, 0);

    if (fd_server < 0)
    {
        perror("socket");
        exit(1);
    }

    server_addr.sin_family 	    = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port 	    = htons(8080);


	if (bind(fd_server, (struct sockaddr *) &server_addr, sizeof(server_addr)) == -1)
	{
		perror("bind");
		close(fd_server);
		exit(1);
	}

	if (listen(fd_server, 5) == -1)
	{
		perror("listen");
		close(fd_server);
		exit(1);
	}

	return fd_server;
}

short accept_client_connection(int srvr) {
    struct sockaddr_in client_addr;
    int fd_client,status;
    socklen_t sin_len = sizeof(client_addr);
    struct addrinfo hints, *servinfo, *p;
    char ipstr[INET_ADDRSTRLEN];
    char hostname[128];


    memset(&hints, 0, sizeof(hints));
	hints.ai_family   = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags	  = AI_PASSIVE;


    fd_client = accept(srvr, (struct sockaddr *) &client_addr, &sin_len);

    if (fd_client == -1) {
        perror("Connection to client failed");
    }

    gethostname(hostname, 128);
    printf("\n%s%s", hostname, " connected w/address ");

    if ((status = getaddrinfo(hostname, NULL, &hints, &servinfo)) == -1) {
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
        exit(1);
    }

    for (p = servinfo; p != NULL; p = p->ai_next)
    {
        struct in_addr  *addr;

        if (p->ai_family == AF_INET)
        {
            struct sockaddr_in *ipv = (struct sockaddr_in *)p->ai_addr;
				addr = &(ipv->sin_addr);
        } else {
            struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
            addr = (struct in_addr *) &(ipv6->sin6_addr);
        }

        inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
    }

    printf("%s\n\n", ipstr);
    freeaddrinfo(servinfo);

    return fd_client;
}

int main(int argc, char *argv [])
{
	char buf[2048];

    int srvr_fd = create_socket();

	for(;;)
	{

        int clnt_fd = accept_client_connection(srvr_fd);

		if (!fork())
		{
			close(srvr_fd);
			memset(buf, 0, 2048);
			read(clnt_fd, buf, 2047);
			printf("%s\n", buf);

			write(clnt_fd, tst, sizeof(tst) -1);
			close(clnt_fd);
			printf("Closing client connection.\n");
			exit(0);
		}

		close(clnt_fd);
	}

}
